#!/usr/bin/env python3

import socket
import random

# Diccionario con los recursos HTML que se pueden pedir
recursos_html = {
    "" : "<html><body><h1>Nothing</h1></body></html>",
    "hola" : "<html><body><h1>Hello</h1></body></html>",
    "adios" : "<html><body><h1>Goodbye</h1></body></html>",
    "gracias" : "<html><body><h1>Thanks</h1></body></html>",
    "suma" : "<html><body><h1>Sum</h1></body></html>",
}

# Clase webApp
class webApp:

    #metodo para analizar la solicitud y obtener de ella la clave del recurso
    def Analyze(self, request):
        received = request.decode("utf-8") #Decodifico la solicitud
        print(received)
        #En este proceso consigo el recurso a pedir, sacando la clave del diccionario
        params = received.split()
        print(params)
        resource = params[1]
        print(f"Resource: {resource}")
        key = resource.split("/")[1]
        print(f"Key: {key}")
        key_lower = key.lower()
        print(f"Key lower: {key_lower}")
        return key_lower

    #metodo para obtener el codigo y la pagina
    def Compute(self, key):
        if key in recursos_html: #Si el recurso existe, obtengo el codigo y la pagina
            codigo = "200 OK"
            html = recursos_html[key]
        else: #Si no existe, obtengo el codigo y la pagina de no encontrado
            codigo = "404 Not Found"
            html = f"<html><body><h1>Not found {key}</h1></body></html>"
        return codigo, html
    def __init__(self, ip, port):

        myServe = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #Creo el socket y le asigno el protocolo y el tipo de socket
        myServe.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #Habilito el reuso de direcciones
        myServe.bind((ip, port)) #Le asigno la direccion y el puerto
        myServe.listen(5) #Acepto 5 conexiones simultaneas
        random.seed()

        while True:
            print("Waiting for connection")
            (recvSocket, address) = myServe.accept() #Acepto la conexion y obtengo el socket y la direccion de la conexion
            print(f"Got connection from  {str(address)}")
            requets = recvSocket.recv(2048) #Recibo la solicitud
            print(requets)
            argAnalysis = self.Analyze(requets)
            codigo, html = self.Compute(argAnalysis)
            recvSocket.send(f"HTTP/1.1 {codigo}\r\n".encode("utf-8"))
            recvSocket.send("Content-Type: text/html\r\n".encode("utf-8"))
            recvSocket.send("\r\n".encode("utf-8"))
            recvSocket.send(html.encode("utf-8"))
            recvSocket.close()


if __name__ == "__main__":
    try:
        web = webApp('', 8080)
    except ConnectionResetError:
        print("Problem with connection")







